/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.github.donoa.KitPlugin;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author Donovan
 */
public class Kit {
    private String type;
    private int lvl;
    @Getter
    private Inventory inven;
    @Getter
    private List<PotionEffect> effects;
    public Kit(){
        this.type = "none";
        this.lvl = 1;
    }
    public Kit(String type, Inventory pinven, List<PotionEffect> effects){
        this.effects=effects;
        this.inven=pinven;
        
    }
    public void saveKit(Player player, String type, int level){
        KitPlugin.getPluginInstance().getConfig().set(player.getName() + ".class", type);
        KitPlugin.getPluginInstance().getConfig().set(player.getName() + ".level", level);
    }
    public void setKit(Player player, String type, int level){
        Inventory pinven = player.getInventory();
        ItemStack ItemStack;
        this.lvl = level;
        if(lvl>KitPlugin.pluginInstance.getConfig().getInt("lvlcap")){
            lvl=KitPlugin.pluginInstance.getConfig().getInt("lvlcap");
        }
        if(lvl<1){
            lvl=1;
        }
        pinven.clear();
        for(PotionEffect effect : player.getActivePotionEffects())
        {
            player.removePotionEffect(effect.getType());
        }
        Enchantment ench1;
        Enchantment ench2;
        switch(type){
            case "ranger":
                ItemStack=new ItemStack(Material.ARROW, 1);
                pinven.addItem(ItemStack);
                ItemStack=new ItemStack(Material.BOW);
                ench1 = new EnchantmentWrapper(51);
                ench2 = new EnchantmentWrapper(48);
                ItemStack.addUnsafeEnchantment(ench1, 1);
                ItemStack.addUnsafeEnchantment(ench2, lvl);
                pinven.addItem(ItemStack);
                ItemStack=new ItemStack(Material.LEATHER_HELMET);
                ItemStack.addUnsafeEnchantment(new EnchantmentWrapper(0), lvl);
                player.getInventory().setHelmet(ItemStack);
                ItemStack=new ItemStack(Material.LEATHER_CHESTPLATE);
                ItemStack.addUnsafeEnchantment(new EnchantmentWrapper(0), lvl);
                player.getInventory().setChestplate(ItemStack);
                ItemStack=new ItemStack(Material.LEATHER_LEGGINGS);
                ItemStack.addUnsafeEnchantment(new EnchantmentWrapper(0), lvl);
                player.getInventory().setLeggings(ItemStack);
                ItemStack=new ItemStack(Material.LEATHER_BOOTS);
                ItemStack.addUnsafeEnchantment(new EnchantmentWrapper(0), lvl);
                player.getInventory().setBoots(ItemStack);
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 2147483647 , 2));
                player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2147483647 , 2));
                player.sendMessage("Level " + String.valueOf(lvl) +" Ranger");
            break;
            case "knight":
                ItemStack=new ItemStack(Material.DIAMOND_SWORD);
                ench1 = new EnchantmentWrapper(20);
                ench2 = new EnchantmentWrapper(16);
                ItemStack.addUnsafeEnchantment(ench1, lvl);
                ItemStack.addUnsafeEnchantment(ench2, lvl);
                pinven.addItem(ItemStack);
                ItemStack=new ItemStack(Material.IRON_HELMET);
                ItemStack.addUnsafeEnchantment(new EnchantmentWrapper(0), lvl);
                player.getInventory().setHelmet(ItemStack);
                ItemStack=new ItemStack(Material.IRON_CHESTPLATE);
                ItemStack.addUnsafeEnchantment(new EnchantmentWrapper(0), lvl);
                player.getInventory().setChestplate(ItemStack);
                ItemStack=new ItemStack(Material.IRON_LEGGINGS);
                ItemStack.addUnsafeEnchantment(new EnchantmentWrapper(0), lvl);
                player.getInventory().setLeggings(ItemStack);
                ItemStack=new ItemStack(Material.IRON_BOOTS);
                ItemStack.addUnsafeEnchantment(new EnchantmentWrapper(0), lvl);
                player.getInventory().setBoots(ItemStack);
                player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 2147483647 , 2));
                player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 2147483647 , 2));
                player.sendMessage("Level " + String.valueOf(lvl)+ " Knight");
            break;
            default:
                Kit k = DBmanager.kits.get(type);
                player.getInventory().setContents(k.getInven().getContents());
                player.addPotionEffects(effects);
                
        }
        this.saveKit(player, type, level);
    }
    
}
