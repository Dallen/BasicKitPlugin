/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.github.donoa.KitPlugin;

import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

/**
 *
 * @author Donovan
 */
public class Commands implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        Player player=(Player) sender;
        if(args.length==0){
            sender.sendMessage("Kits: Ranger, Knight");
            return true;
        }else if(args[0].equalsIgnoreCase("create")){
            Kit k = new Kit(args[1],player.getInventory(), (List<PotionEffect>) player.getActivePotionEffects());
            DBmanager.kits.put(args[1], k);
        }else{
            if(!DBmanager.kits.containsKey(args[0])){
                player.sendMessage("no such kit");
                return false;
            }
            Kit pkit = DBmanager.kits.get(args[0]);
            int lvl=1;
                if(args.length==2){
                    lvl=Integer.parseInt(args[1]);
                }
                else{
                    lvl=1;
                }
            pkit.setKit(player, "ranger", lvl);
        }
        return false;
    }
    
}