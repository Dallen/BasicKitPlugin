/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.github.donoa.KitPlugin;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 *
 * @author Donovan
 */
class Listeners implements Listener {
    @EventHandler
    public void onLogin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        if(!KitPlugin.getPluginInstance().getConfig().isSet(player.getName())){
            KitPlugin.getPluginInstance().getConfig().set(player.getName()+".level", 1);
            KitPlugin.getPluginInstance().getConfig().set(player.getName()+".class", "none");
            player.sendMessage("Welcome, to start your adventure type /kit");
        }
        if(!KitPlugin.getPluginInstance().getConfig().getString(player.getName()+".class").equalsIgnoreCase("none")){
            switch(KitPlugin.getPluginInstance().getConfig().getString(player.getName()+".class")){
                case "ranger":
                    player.sendMessage(ChatColor.AQUA + "You are a Ranger");
                break;
                case "knight":
                    player.sendMessage(ChatColor.AQUA + "You are a Knight");
                break;
            }
        }
    }
    
}
