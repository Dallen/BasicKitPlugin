package io.github.donoa.KitPlugin;

import lombok.Getter;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class KitPlugin extends JavaPlugin{
    @Getter
    static KitPlugin pluginInstance;

    @Override
    public void onEnable(){
        pluginInstance = this;
        getCommand("kit").setExecutor(new Commands());
        getLogger().info("Enabled KitPlugin from log");
        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(new Listeners(), this);
        this.reloadConfig();
        getLogger().info("tester");
    }
    @Override
    public void onDisable(){
        getLogger().info("Disabled KitPlugin from log");
        this.saveConfig();
    }
}
